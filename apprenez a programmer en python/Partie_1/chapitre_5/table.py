uswer_answer= True

def table(nbr, limite):
	i = 1
	while i <= limite:
		resulta = i * nbr
		print ( nbr , "*", i, " = ", resulta)
		i+=1

while uswer_answer != False:
	nbr = input("quel table voulez vous afficher ?")
	limite = input ("jusque quel chiffre l'afficher ?")
	try:
		nbr = int(nbr)
		limite = int(limite)
	except ValueError:
		print("vous devez entrez des chiffres.")
	print (table(nbr, limite))
	uswer_answer = input("Voulez vous afficher une autre table ? 'O/N' ")
	if uswer_answer.lower() == "o":
		uswer_answer = True
	elif uswer_answer.lower() == "n":
		uswer_answer = False