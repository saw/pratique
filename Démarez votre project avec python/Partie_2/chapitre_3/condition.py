# la condition if 

quotes = ["Ecoutez-moi, Monsieur Shakespeare, nous avons beau être ou ne pas être, nous sommes !", "On doit pouvoir choisir entre s'écouter parler et se faire entendre."]

characters = ["alvin et les Chipmunks", "Babar", "betty boop", "calimero", "casper", "le chat potté", "Kirikou"]

if user_answer == "B": # premiére condition du programme
	pass
elif user_answer == "C" # seconde condition du programme
	print ("seconde condition")
else: # sinon alors effectuer action en dessous.
	print("sinon")

