quotes = ["Ecoutez-moi, Monsieur Shakespeare, nous avons beau être ou ne pas être, nous sommes !", "On doit pouvoir choisir entre s'écouter parler et se faire entendre."]

characters = ["alvin et les Chipmunks", "Babar", "betty boop", "calimero", "casper", "le chat potté", "Kirikou"]

# afficher citation au hasard
# si user_answer est "B"
	#dans ce cas la quitte le programme
#else
	#dans ce cas la montre une nouvelle citation.

# opérateur: 2 types : comparaison, mathématique, logique.

# comparaison renvoie un booleens

"""
    < strictement inférieur
    > strictement supérieur
    <= inférieur ou égal
    >= supérieur ou égal
    == égal
    != différent
    <> différent, on utilisera de préférence !=
    X is Y : X et Y représentent le même objet.
    X is not Y : X et Y ne représentent pas le même objet
"""

# opérateur mathématique: 

"""
	+
	-
	*
	/
	% 13%7 = 6 
"""

# operateur logique : 

"""
    X or Y : OU logique.
    Si X est évalué à True, alors l'expression est True et Y n'est pas évalué.
    Sinon, l'expression est évaluée à la valeur booléenne de Y.
    X and Y : ET logique.
    Si X est évalué à False, alors l'expression est False et Y n'est pas évalué.
    Sinon, l'expression est évaluée à la valeur booléenne de Y.
    not X : NON logique.
    Evalué à la valeur booléenne opposée de X.
"""
