/* 
Activité 1
*/

// Liste des liens Web à afficher. Un lien est défini par :
// - son titre
// - son URL
// - son auteur (la personne qui l'a publié)
var listeLiens = [
    {
        titre: "So Foot",
        url: "http://sofoot.com",
        auteur: "yann.usaille"
    },
    {
        titre: "Guide d'autodéfense numérique",
        url: "http://guide.boum.org",
        auteur: "paulochon"
    },
    {
        titre: "L'encyclopédie en ligne Wikipedia",
        url: "http://Wikipedia.org",
        auteur: "annie.zette"
    }
];

// TODO : compléter ce fichier pour ajouter les liens à la page web

contenu = document.querySelector("#contenu");
bouttonAjout = document.querySelector("#ajouter");
formulaire = document.querySelector("#formulaire");
valideFormulaire = document.querySelector("#valideForm");
var x = "http://";

function update() {
    // on vide la liste
    contenu.innerHTML = "";

    // on reconstrui tout
    listeLiens.forEach(function(lien) {
        
        var titre = lien.titre + " ";
        var url = lien.url;
        var auteur = lien.auteur;
    
        //creation des balise
        divElt = document.createElement("div");
        titreElt = document.createElement("p");
        aElt = document.createElement("a");
        spanElt = document.createElement("span");
    
        //contenu des elements
        divElt.classList = "lien";
    
        titreElt.textContent = lien.titre + " ";
        titreElt.style.color = "#428bca";
        titreElt.style.fontWeight = "bold";
        titreElt.style.display = "inline";
    
        aElt.textContent = lien.url;
        aElt.href = lien.url;
        aElt.style.textDecoration = "none";
        aElt.style.color = "black";

        spanElt.textContent = "Add by " + auteur;
        spanElt.style.display = "block";
    
        //ajout dans le dom
    
        contenu.appendChild(divElt);
        divElt.appendChild(titreElt);
        divElt.appendChild(aElt);
        divElt.appendChild(spanElt);
        
    })

}

function afficheLeNomLien (nom) {
    var div = document.createElement("div");
    div.id="afficheLeNom";
    div.textContent = nom + " a été ajouté";
    document.querySelector("h1").after(div);
}

update();

bouttonAjout.addEventListener("click", function (){
    bouttonAjout.style.display = "none";
    formulaire.style.display = "block";
})

valideFormulaire.addEventListener("click", function(){
    var nouvelleItem = new Object();
    nouvelleItem.titre = document.querySelector("#titreContent").value;
    nouvelleItem.url = document.querySelector("#lienContent").value;
    nouvelleItem.auteur = document.querySelector("#prenom").value;
    if (document.querySelector("#prenom").value) {
        if(document.querySelector("#titreContent").value) {
            if(document.querySelector("#lienContent").value){
                var chaine = document.querySelector("#lienContent").value;
                if (chaine.indexOf("http://") == -1 && chaine.indexOf("https://") == -1){
                    nouvelleItem.url = x + chaine ;
                }
                    listeLiens.unshift(nouvelleItem);
                    formulaire.style.display = "none";
                    bouttonAjout.style.display = "block";
                    afficheLeNomLien(document.querySelector("#titreContent").value);
                    update();
                    setTimeout (function(){
                    document.querySelector("body").removeChild(document.querySelector("#afficheLeNom"));
                    },2000);
                    document.querySelector("#prenom").value = "";
                    document.querySelector("#titreContent").value = "";
                    document.querySelector("#lienContent").value = "";
                
            }else alert("Entrez une url");
        }else alert("Entrez un titre");
    }else alert("Entrez un prenom");
    
})
