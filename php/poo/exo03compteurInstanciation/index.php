<?php 
class Personnage {
    
    static private $_compteur = 0;
    
    public function __construct(){
        self::$_compteur ++;
    }
    public static function getCompteur(){
        return self::$_compteur;
    }
}

$perso1 = new Personnage;
$perso2 = new Personnage;
$perso3 = new Personnage;
$perso4 = new Personnage;
$perso5 = new Personnage;

echo Personnage::getCompteur();

?>